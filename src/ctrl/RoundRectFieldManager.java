package ctrl;

import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.container.VerticalFieldManager;

public class RoundRectFieldManager extends VerticalFieldManager
{
	private static final int CORNER_RADIUS = 12;
	
	public RoundRectFieldManager()
	{
		super(USE_ALL_WIDTH);
	}
	
	public void paintBackground(Graphics g)
	{
		int oldColor=g.getColor();
		g.setColor(Color.WHITE);
		g.fillRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
		g.setColor(Color.LIGHTGRAY);
		g.drawRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
		
		g.setColor(oldColor);
	}
}