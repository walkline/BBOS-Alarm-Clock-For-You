package code;

import java.util.Hashtable;

import net.rim.device.api.util.Persistable;

public class WorldClockSettings extends Hashtable implements Persistable
{
	private Hashtable _elements;
	
	public WorldClockSettings() 
	{
		_elements=new Hashtable(2);
	}
	
	public void setCityName(String cityName)
	{
		_elements.put("city_name", cityName);
	}
	
	public void setTimeZone(String timeZone)
	{
		_elements.put("time_zone", timeZone);
	}

	public void setCurrentIndex(String index)
	{
		_elements.put("curr_index", index);
	}

	public String getCityName()
	{
		return (String) _elements.get("city_name");
	}
	
	public String getTimeZone()
	{
		return (String) _elements.get("time_zone");
	}
	
	public int getCurrentIndex()
	{
		return Integer.valueOf((String) _elements.get("curr_index")).intValue();
	}
}