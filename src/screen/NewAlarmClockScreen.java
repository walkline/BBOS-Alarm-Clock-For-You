package screen;

import localization.alarmclockResource;
import net.rim.device.api.i18n.ResourceBundle;
import net.rim.device.api.ui.container.MainScreen;

public class NewAlarmClockScreen extends MainScreen implements alarmclockResource
{
	private static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
	
	public NewAlarmClockScreen()
	{
		super(MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR);
		
		setTitle(getResString(TITLE_ADD_NEW_ALARMCLOCK));
	}
	
	private String getResString(int key)
	{
		return _resources.getString(key);
	}
}