package code;

import java.util.Vector;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.UiApplication;
import screen.AlarmClockScreen;

public class AlarmClock extends UiApplication
{
	private boolean needCleanData=false;
	private static PersistentObject _store;
	
    public static void main(String[] args)
    {
    	AlarmClock theApp = new AlarmClock();       
    	theApp.enterEventDispatcher();
    }

    public AlarmClock()
    {        
    	if(needCleanData)
    	{
        	synchronized(_store)
    		{
    			_store.setContents(new Vector());
    			_store.forceCommit();
    		}
    	}
    	
        pushScreen(new AlarmClockScreen());
    }
    
    static
	{
    	_store=PersistentStore.getPersistentObject(0xaa041103a4a17b66L); //blackberry_alarmclock_worldclock_settings_id
	}
}