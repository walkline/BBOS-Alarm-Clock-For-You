package screen;

import java.util.Vector;

import localization.alarmclockResource;
import net.rim.device.api.i18n.ResourceBundle;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.NullField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.MainScreen;
import code.AppSettings;
import ctrl.ForegroundManager;
import ctrl.ListStyleButtonField;
import ctrl.ListStyleButtonSet;
import ctrl.RoundRectFieldManager;

public class SettingsScreen extends MainScreen implements alarmclockResource
{
	private ForegroundManager _foreground;
	private ObjectChoiceField fontSizeChoice;
	private ListStyleButtonSet _buttonSet=new ListStyleButtonSet();
	private ListStyleButtonField link_alarmClock;
	private ListStyleButtonField link_worldClock;
	private ListStyleButtonField link_birthdayReminder;
	
	private int settingsIndex=1;
	private boolean canUpdateSettings=false;
	private boolean _isModified=false;
	private static Vector _data;
	private static PersistentObject _store;
	private static AppSettings appSettings=new AppSettings();
	private static ResourceBundle _resources = ResourceBundle.getBundle(BUNDLE_ID, BUNDLE_NAME);
	
	public SettingsScreen()
	{
		super(USE_ALL_HEIGHT | NO_VERTICAL_SCROLL);
		setTitle(getResString(SETTINGS_TITLE));
		
		RoundRectFieldManager vfm1=new RoundRectFieldManager();
		RoundRectFieldManager vfm2=new RoundRectFieldManager();
		RoundRectFieldManager vfm3=new RoundRectFieldManager();
		RoundRectFieldManager vfm4=new RoundRectFieldManager();
		
		_foreground=new ForegroundManager(0);
		
		String[] fontSize=getResStringArray(SETTINGS_FONTSIZE_ARRAY);
		fontSizeChoice=new ObjectChoiceField(getResString(LABEL_CAPTION_SELECT_FONTSIZE), fontSize) {
	        protected void fieldChangeNotify(int context)
	        {
	        	try {
		        	link_alarmClock.setFontSize(fontSizeChoice.getSelectedIndex());
		        	link_worldClock.setFontSize(fontSizeChoice.getSelectedIndex());
		        	link_birthdayReminder.setFontSize(fontSizeChoice.getSelectedIndex());
		        	fontSizeChoice.setDirty(true);
				} catch (Exception e) {}
			}
		};

    	LabelField display=new LabelField(getResString(LABEL_TITLE_DISPLAY), USE_ALL_WIDTH | LabelField.ELLIPSIS) {
    		protected void paint(Graphics g)
    		{
    			g.setColor(Color.DARKGRAY);
    			super.paint(g);	
    		}
    	};
    	display.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt));
    	vfm1.add(display);
    	vfm1.add(new SeparatorField());
    	vfm1.add(fontSizeChoice);
		vfm1.setMargin(5,5,5,5);
		vfm1.setPadding(3,5,5,5);
		
    	LabelField shortcut=new LabelField("Cycles", USE_ALL_WIDTH | LabelField.ELLIPSIS) {
    		protected void paint(Graphics g)
    		{
    			g.setColor(Color.DARKGRAY);
    			super.paint(g);	
    		}
    	};
    	shortcut.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt));
    	vfm2.add(shortcut);
    	vfm2.add(new SeparatorField());
		vfm2.setMargin(5,5,5,5);
		vfm2.setPadding(3,5,5,5);

    	LabelField lblNickname=new LabelField("Alert", USE_ALL_WIDTH | LabelField.ELLIPSIS) {
    		protected void paint(Graphics g)
    		{
    			g.setColor(Color.DARKGRAY);
    			super.paint(g);	
    		}
    	};
    	lblNickname.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt));
    	vfm3.add(lblNickname);
    	vfm3.add(new SeparatorField());
		vfm3.setMargin(5,5,5,5);
		vfm3.setPadding(3,5,5,5);

		link_alarmClock=new ListStyleButtonField("Alarm at 6:30", "06:30", "Weekdays", true);
		link_worldClock=new ListStyleButtonField("Beijing", "18:30", "2013-05-30 (+8)");
		link_birthdayReminder=new ListStyleButtonField("Kids birthday", "1990-12-24 (十月初八)", "More Days", "128");
		_buttonSet.add(link_alarmClock);
		_buttonSet.add(link_worldClock);
		_buttonSet.add(link_birthdayReminder);
		
    	LabelField sample=new LabelField(getResString(LABEL_TITLE_SAMPLE), USE_ALL_WIDTH | LabelField.ELLIPSIS) {
    		protected void paint(Graphics g)
    		{
    			g.setColor(Color.DARKGRAY);
    			super.paint(g);	
    		}
    	};
    	sample.setFont(Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt));
    	vfm4.add(sample);
    	vfm4.add(new SeparatorField());
    	vfm4.add(_buttonSet);
		vfm4.setMargin(5,5,5,5);
		vfm4.setPadding(3,5,5,5);
		
		_foreground.add(vfm1);
		_foreground.add(vfm2);
		_foreground.add(vfm3);
		_foreground.add(new NullField(NON_FOCUSABLE));
		_foreground.add(new NullField(NON_FOCUSABLE));
		_foreground.add(vfm4);
		add(_foreground);

		synchronized(_store)
		{
			if(!_data.isEmpty())
			{
		    	for(int i=0; i<_data.size(); i++)
		    	{
		    		if(_data.elementAt(i) instanceof AppSettings)
		    		{
		    			appSettings=(AppSettings) _data.elementAt(i);
		    			settingsIndex=appSettings.getFontSize();
		    			
		    			i=_data.size()+1;
		    		}
		    	}
			}
		}
		
		link_alarmClock.setFontSize(settingsIndex);
		link_worldClock.setFontSize(settingsIndex);
		link_birthdayReminder.setFontSize(settingsIndex);
		fontSizeChoice.setSelectedIndex(settingsIndex);
		fontSizeChoice.setDirty(false);
	}
	
    static
   	{
   		_store=PersistentStore.getPersistentObject(0xaa041103a4a17b66L); //blackberry_alarmclock_worldclock_settings_id
   		
   		synchronized(_store)
   		{
   			if(_store.getContents()==null)
   			{
   				_store.setContents(new Vector());
   				_store.forceCommit();
   			}
   		}
   		
   		try {
   			_data=new Vector();
   			_data=(Vector) _store.getContents();
   		} catch (final Exception e) {}
   	}
	
	private String getResString(int key)
	{
		return _resources.getString(key);
	}
	
    private String[] getResStringArray(int key)
	{
    	return _resources.getStringArray(key);
	}
    
	public boolean onSave()
	{
		if(!_data.isEmpty())
		{
			for(int i=0; i<_data.size(); i++)
			{
				if(_data.elementAt(i) instanceof AppSettings)
				{
					appSettings.setFontSize(String.valueOf(fontSizeChoice.getSelectedIndex()));
					_data.setElementAt(appSettings, i);
					
					i=_data.size()+1;
					canUpdateSettings=true;
				}
			}
		}
		
		if(!canUpdateSettings)
		{
			appSettings.setFontSize(String.valueOf(fontSizeChoice.getSelectedIndex()));
			_data.addElement(appSettings);
		}
		
		synchronized(_store)
		{
			_store.setContents(_data);
			_store.commit();
		}
		
		_isModified=true;
		
		return true;
	}
	
	public boolean isModified()
	{
		return _isModified;
	}
	
	public int getFontSize()
	{
		return fontSizeChoice.getSelectedIndex();
	}
}