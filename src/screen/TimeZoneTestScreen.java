package screen;

import java.util.TimeZone;

import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.util.MathUtilities;
import net.rim.device.api.util.TimeZoneUtilities;

public class TimeZoneTestScreen extends MainScreen
{
    public TimeZoneTestScreen()
    {
        super( MainScreen.VERTICAL_SCROLL | MainScreen.VERTICAL_SCROLLBAR );
        setTitle("My Test App");
        
        final ObjectChoiceField timeZoneID=new ObjectChoiceField("TimeZone ID:", TimeZone.getAvailableIDs()) {
            protected void fieldChangeNotify(int context)
            {
            	Dialog.alert("ID: " + TimeZone.getTimeZone((String) getChoice(getSelectedIndex())).getID() + "\n" +
            				"use DST: " + TimeZone.getTimeZone((String) getChoice(getSelectedIndex())).useDaylightTime() + "\n" +
            				"timezone1: " + ((double) TimeZone.getTimeZone((String) getChoice(getSelectedIndex())).getRawOffset())/3600000 + "\n" + 
            				"timezone2: " + MathUtilities.round(((double) TimeZone.getTimeZone((String) getChoice(getSelectedIndex())).getRawOffset())/3600000));
            }
        };
        
        add(timeZoneID);
        add(new ObjectChoiceField("TimeZones:", TimeZoneUtilities.getAvailableTimeZones()));
        add(new ObjectChoiceField("Display Name Long:", TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.LONG)));
        add(new ObjectChoiceField("Display Name Short:", TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.SHORT)));
        add(new LabelField("TimeZone ID: " + TimeZone.getAvailableIDs().length + "\n" +
        					"TimeZones: " + TimeZoneUtilities.getAvailableTimeZones().length + "\n" +
        					"Long Name: " + TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.LONG).length + "\n" +
        					"Short Name: " + TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.SHORT).length));
    }
}