package code;

import java.util.TimeZone;

import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.XYEdges;
import net.rim.device.api.ui.decor.Background;
import net.rim.device.api.ui.decor.BackgroundFactory;
import net.rim.device.api.ui.decor.Border;
import net.rim.device.api.ui.decor.BorderFactory;
import net.rim.device.api.util.TimeZoneUtilities;

public class Config
{
	public static final String APP_SHORTCUT="(R\u0332)";
	public static final String APP_TITLE="Alarm Clock for you";
	public static final String BLACKBERRY_WORLD_VENDOR_HOMEPAGE="http://appworld.blackberry.com/webstore/vendor/69061";
			
	public static final String titleIconName=Display.getWidth()<640 ? "titleIcon.png" : "titleIcon_large.png";
	
	public static final Border border_Transparent=BorderFactory.createRoundedBorder(new XYEdges(16,16,16,16), Color.BLACK, 200, Border.STYLE_FILLED);
	public static final Background bg_Transparent=BackgroundFactory.createSolidTransparentBackground(Color.BLACK, 200);

	//public static final Font FONT_APP_TITLE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_PANEVIEW_TITLE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+3,Ui.UNITS_pt);
	public static final Font FONT_SETTINGS=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	//Small Fonts
	public static final Font FONT_TITLE_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_TIME_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+2,Ui.UNITS_pt);
	public static final Font FONT_DAYS_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-2,Ui.UNITS_pt);
	public static final Font FONT_DATE_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-2,Ui.UNITS_pt);
	public static final Font FONT_DATEOFBIRTH_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-2,Ui.UNITS_pt);
	public static final Font FONT_NEXTAGETIPS_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-2,Ui.UNITS_pt);
	public static final Font FONT_REMAININGDAYS_SMALL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+2,Ui.UNITS_pt);
	//Normal Fonts
	public static final Font FONT_TITLE_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+1,Ui.UNITS_pt);
	public static final Font FONT_TIME_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+3,Ui.UNITS_pt);
	public static final Font FONT_DAYS_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	public static final Font FONT_DATE_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	public static final Font FONT_DATEOFBIRTH_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	public static final Font FONT_NEXTAGETIPS_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)-1,Ui.UNITS_pt);
	public static final Font FONT_REMAININGDAYS_NORMAL=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+3,Ui.UNITS_pt);
	//Large Fonts
	public static final Font FONT_TITLE_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+3,Ui.UNITS_pt);
	public static final Font FONT_TIME_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+6,Ui.UNITS_pt);
	public static final Font FONT_DAYS_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_DATE_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_DATEOFBIRTH_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_NEXTAGETIPS_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt),Ui.UNITS_pt);
	public static final Font FONT_REMAININGDAYS_LARGE=Font.getDefault().derive(Font.PLAIN, Font.getDefault().getHeight(Ui.UNITS_pt)+4,Ui.UNITS_pt);
			
	public static final int FONT_SIZE_TITLE=0;
	public static final int FONT_SIZE_TIME=1;
	public static final int FONT_SIZE_DAYS=2;
	
	public static int MILLIS_OF_HOUR=3600000; //60*60*1000
	
	public static final String[] TIMEZONE_DESCRIPTIONS=TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.LONG);
	public static final String[] TIMEZONE_DISPLAY_NAMES=TimeZoneUtilities.getDisplayNames(TimeZoneUtilities.SHORT);
	public static final String[] TIMEZONE_IDS=TimeZone.getAvailableIDs();
	public static final Background bgColor_Gradient=BackgroundFactory.createLinearGradientBackground(Color.GRAY, Color.GRAY, Color.BLACK, Color.BLACK);
}