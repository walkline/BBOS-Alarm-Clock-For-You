package code;

import java.util.Hashtable;

import net.rim.device.api.util.Persistable;

public class AlarmClockSettings extends Hashtable implements Persistable
{
	private Hashtable _elements;
	
	public AlarmClockSettings() 
	{
		_elements=new Hashtable(1);
	}
	
	public void setFontSize(String fontSize)
	{
		_elements.put("font_size", fontSize);
	}
	
	public int getFontSize()
	{
		return Integer.valueOf((String) _elements.get("font_size")).intValue();
	}
}