package code;

import java.util.Hashtable;

import net.rim.device.api.util.Persistable;

public class BirthdayReminderSettings extends Hashtable implements Persistable
{
	private Hashtable _elements;
	
	public BirthdayReminderSettings() 
	{
		_elements=new Hashtable(3);
	}
	
	public void setBirthdayName(String birthdayName)
	{
		_elements.put("birthday_name", birthdayName);
	}
	
	public void setBirthdayDate(String birthdayDate)
	{
		_elements.put("birthday_date", birthdayDate);
	}
	
	public void setIsUsingLunar(String usingLunar)
	{
		_elements.put("using_lunar", usingLunar);
	}
	
	public String getBirthdayName()
	{
		return (String) _elements.get("birthday_name");
	}
	
	public long getBirthdayDate()
	{
		return Long.parseLong((String) _elements.get("birthday_date"));
	}
	
	public boolean getIsUsingLunar()
	{
		String string=(String) _elements.get("using_lunar");
		
		return string.equals("1") ? true : false;
	}
}